package main;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;

import pitt.search.lucene.FilePositionDoc;
import pitt.search.lucene.IndexFilePositions;
import pitt.search.lucene.PorterAnalyzer;
import pitt.search.semanticvectors.FlagConfig;

public class ExtractMessages {

	///Users/mfamith/Desktop/westbury corpus/WestburyLab.NonRedundant.UsenetCorpus.txt

	//vaccine, vaccinate, vaccination, immunogen, (from Wordnet, ConceptNet)



	private static String westburyCorpus = "/Users/mfamith/Desktop/westbury corpus/WestburyLab.NonRedundant.UsenetCorpus.txt";
	private static long totalMessages = 0;
	private static long totalVacDocuments = 0;
	public ExtractMessages(){

		//IndexFilePositions tester;
		//tester.

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		SparkConf sparkConf = new SparkConf().setMaster("local[6]").set("spark.driver.maxResultSize", "3g").setAppName("Extraction");
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);
		//sparkContext.getConf().set("spark.driver.maxResultSize", "2g");

		JavaRDD<String> input = sparkContext.textFile(westburyCorpus);

		JavaRDD<String> messagesRDD = input.mapPartitions(new ParseDocuments());
		totalMessages = messagesRDD.count();
		//System.out.println("TOTAL MESSAGES: " + messagesRDD.count());

		JavaRDD<String> layVacDocsRDD = messagesRDD.filter(new SelectVacDocuments());
		//filter documents by vaccine terms
		totalVacDocuments = layVacDocsRDD.count();
		System.out.println("TOTAL VAC DOCUMENTS: " + totalVacDocuments + " and TOTAL MESSAGE: " + totalMessages);
		//126870 with just vaccine
		//TOTAL VAC DOCUMENTS: 136798 and TOTAL MESSAGE: 32880805
		
		
		

		//**************************************//
		//******* CREATE LUCENE INDEX **********//
		//**************************************//

		//JavaRDD<Document> luceneDocs;

		Path indexDir = FileSystems.getDefault().getPath("/Users/mfamith/Desktop/semvec/positional_index");
		IndexWriter writer;
		FlagConfig flagConfig = null;

		///Users/mfamith/Desktop/semvec
		flagConfig = FlagConfig.getFlagConfig(new String[]{"/Users/mfamith/Desktop/semvec"});

		if (flagConfig.luceneindexpath().length() > 0) {
			indexDir = FileSystems.getDefault().getPath(flagConfig.luceneindexpath());
		}

		try {

			Analyzer analyzer = flagConfig.porterstemmer() ? new PorterAnalyzer() : 
				new StandardAnalyzer(CharArraySet.EMPTY_SET);

			IndexWriterConfig writerConfig = new IndexWriterConfig(analyzer);
			

			writer = new IndexWriter(FSDirectory.open(indexDir), writerConfig);
			
			final File docDir = new File(flagConfig.remainingArgs[0]);
			
			if (!docDir.exists() || !docDir.canRead()) {
				writer.close();
				throw new IOException ("Document directory '" + docDir.getAbsolutePath() +
						"' does not exist or is not readable, please check the path");
			}

			//writer.addDocument(FilePositionDoc.Document(file));
			//List<String>docFiles = layVacDocsRDD.collect();
			System.out.println("Indexing to directory '");
			
			//layVacDocsRDD.foreach(new AddToWriter(writer));
			//layVacDocsRDD.foreachPartition(new AddToWriter(writer));
			int count =0;
			for(String message : layVacDocsRDD.collect()){
				writer.addDocument(StringPositionDoc.Document(message, count));
				System.out.println("Added " + count++ +" documents");
			}
			
			System.out.println("Done adding to directory");
			/*layVacDocsRDD.forEach(
					new VoidFunction<Iterator<String>>(){

						@Override
						public void call(Iterator<String> messages) throws Exception {
							// TODO Auto-generated method stub
							
							while(messages.hasNext()){
								String message = messages.next();
								///File file = new File(message);
								writer.addDocument(StringPositionDoc.Document(message));
							}
							
						}
						
					}
					);*/
			
			/*for (String docFile : docFiles){
				//File file = new File(docFile);
				writer.addDocument(StringPositionDoc.Document(docFile));
			}*/

			writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//**************************************//
		//******* END LUCENE INDEX **********//
		//**************************************//

	}

}
