package main;

import org.apache.spark.api.java.function.Function;

public class SelectVacDocuments implements Function<String, Boolean> {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public Boolean call(String doc) throws Exception {
		// TODO Auto-generated method stub
		
		//vaccine, vaccinate, vaccination, immunogen, (from Wordnet, ConceptNet)
		return (doc.contains("vaccine") || 
				doc.contains("vaccinate") ||
				doc.contains("vaccination") ||
				doc.contains("immunogen"));
	}

}
