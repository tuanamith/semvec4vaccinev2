package main;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

import pitt.search.semanticvectors.FlagConfig;
import pitt.search.semanticvectors.LuceneUtils;
import pitt.search.semanticvectors.SearchResult;
import pitt.search.semanticvectors.VectorSearcher;
import pitt.search.semanticvectors.VectorStoreRAM;
import pitt.search.semanticvectors.vectors.ZeroVectorException;

public class SearchUtil {

	public static String DEFAULT_VECTOR_FILE = "termvectors.bin";

	public static void main(String[] args) throws IOException, ZeroVectorException {
		// TODO Auto-generated method stub

		String vectorStoreName = DEFAULT_VECTOR_FILE;

		FlagConfig defaultFlagConfig = FlagConfig.getFlagConfig(null);
		VectorStoreRAM searchVectorStore = VectorStoreRAM.readFromFile(defaultFlagConfig, vectorStoreName);
		LuceneUtils luceneUtils = null; 

		System.out.println("Enter a query term:");
		Scanner sc = new Scanner(System.in);
		String queryString = sc.nextLine();

		VectorSearcher searcher = new VectorSearcher.VectorSearcherCosine(
				searchVectorStore, searchVectorStore, luceneUtils, defaultFlagConfig, queryString.split("\\s+"));
		LinkedList<SearchResult> results = searcher.getNearestNeighbors(10);

		for (SearchResult result : results) {
			System.out.println(result.getScore() + ":" + result.getObjectVector().getObject());
		}

	}

}
