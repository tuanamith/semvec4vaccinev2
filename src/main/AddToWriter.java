package main;

import java.util.Iterator;

import org.apache.lucene.index.IndexWriter;
import org.apache.spark.api.java.function.VoidFunction;

public class AddToWriter implements VoidFunction <Iterator<String>> {
	
	private IndexWriter writer;
	
	public AddToWriter(IndexWriter writer){
		this.writer = writer;
	}

	/*@Override
	public void call(String message) throws Exception {
		// TODO Auto-generated method stub
		writer.addDocument(StringPositionDoc.Document(message));
	}
*/
	@Override
	public void call(Iterator<String> stuff) throws Exception {
		// TODO Auto-generated method stub
		
		while(stuff.hasNext()){
			String message = stuff.next();
			writer.addDocument(StringPositionDoc.Document(message, 0));
		}
		
	}

	

}
