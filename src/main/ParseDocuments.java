package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;

public class ParseDocuments implements FlatMapFunction<Iterator<String>, String>{
	
	//---END.OF.DOCUMENT--- delimiter
	
	private String delimiter = "---END.OF.DOCUMENT---";

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	

	@Override
	public Iterator<String> call(Iterator<String> lines) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<String>layVacDocs = new ArrayList<String>();
		StringBuilder sb = new StringBuilder("");
		
		while(lines.hasNext()){
			String line = lines.next();
			if(line.contains(delimiter)){
				layVacDocs.add(sb.toString());
				sb = new StringBuilder("");
			}
			else{
				if(line !=null) sb.append(line);
			}
			
		}
		return layVacDocs.iterator();
		
		
		
		
		//return layVacDocs;
	}

}
